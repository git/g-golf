@c -*-texinfo-*-
@c This is part of the GNU G-Golf Reference Manual.
@c Copyright (C) 2016 - 2025 Free Software Foundation, Inc.
@c See the file g-golf.texi for copying conditions.


@c @part I. Introduction

@node Introduction
@unnumbered I. Introduction

@menu
* About G-Golf::
@c * Description::
@c * What else::
@c * Savannah::
* Obtaining and installing G-Golf::
* Contact Information::
* Reporting Bugs::
@end menu


@node About G-Golf
@section About G-Golf

G-Golf @*
GNOME: (Guile Object Library for).


@subheading Description

G-Golf is a Guile@footnote{GNU @uref{@value{UGUILE}, Guile}@*an
interpreter and compiler for the @uref{@value{USCHEME}, Scheme}
programming language.} Object Library for @uref{@value{UGNOME}, GNOME}.

G-Golf is a tool to develop fast and feature-rich graphical
applications, with a clean and recognizable look and feel. Here is an
overview of the @uref{@value{UGNOME-Libraries}, GNOME platform
libraries}, accessible using G-Golf.

In particular, @uref{@value{ULIBADWAITA}, libadwaita} provides a number
of widgets that change their layout based on the available space. This
can be used to make applications adapt their UI between desktop and
mobile devices. The @uref{@value{UGNOME-Web}, GNOME Web} (best known
through its code name, Epiphany, is a good example of such an adaptive
UI.

G-Golf uses @uref{@value{UGLIB}, Glib}, @uref{@value{UGOBJECT}, GObject}
and @uref{@value{UGI-OVERVIEW}, GObject Introspection}.  As it imports a
@uref{@value{UGI-OVERVIEW}, Typelib} (a GObject introspectable library),
G-Golf defines GObject classes as GOOPS@footnote{The Guile Object
Oriented System, @xref{GOOPS,,, guile, The GNU Guile Reference Manual}}
classes.  GObject methods are defined and added to their corresponding
generic function. Simple functions are defined as scheme procedures.

Here is an example, an excerpt taken from the peg-solitaire game, that
shows the implementation, for the peg-solitaire game, of the
GtkApplication activate signal callback in G-Golf:

@lisp
(define (activate app)
  (let ((window (make <gtk-application-window>
                  #:title "Peg Solitaire"
                  #:default-width 420
                  #:default-height 420
                  #:application app))
        (header-bar (make <gtk-header-bar>))
        (restart (make <gtk-button>
                   #:icon-name "view-refresh-symbolic")))

    (connect restart
             'clicked
             (lambda (bt)
               (restart-game window)))

    (set-titlebar window header-bar)
    (pack-start header-bar restart)
    (create-board window)
    (show window)))
@end lisp

G-Golf comes with some examples, listed on the
@uref{@value{UG-GOLF-learn}, learn page} of the G-Golf web site. Each
example comes with a screenshot and has a link that points to its source
code, in the G-Golf sources @uref{@value{UG-GOLF-GIT}, repository}.

@subheading Savannah

GNU G-Golf also has a project page on @uref{@value{UG-GOLF-SAVANNAH},
Savannah}.


@node Obtaining and installing G-Golf
@section Obtaining and installing G-Golf

G-Golf can be obtained from the following archive site
@uref{@value{UG-GOLF-RELEASES}}.  The file will be named
g-golf-version.tar.gz. The current version is @value{VERSION}, so the
file you should grab is:

@tie{}@tie{}@tie{}@tie{}@uref{@value{UG-GOLF-LATEST}}


@subheading Dependencies

@strong{* Main Dependencies}

G-Golf needs the following software to run:

@itemize @bullet
@item
Autoconf @geq{} 2.69

@item
Automake @geq{} 1.14

@item
Makeinfo @geq{} 6.6

@item
@uref{@value{UGUILE}, Guile} 2.0 (@geq{} 2.0.14), 2.2 or 3.0 (@geq{} 3.0.7)

@item
@uref{@value{UGLIB}, GLib-2.0} @geq{} 2.73.0

@item
@uref{@value{UGOBJECT}, GObject-2.0} @geq{} 2.73.0

@item 
@uref{@value{UGI}, GObject-Introspection-1.0} @geq{} 1.72.0
@end itemize


@strong{* Test-Suite Dependencies}

G-Golf currently needs the following additional software to run its
test-suite:

@itemize @bullet
@c @item
@c @uref{@value{UCLUTTER}, Clutter-1.0} @geq{} 1.24.0

@item
@uref{@value{UGUILE-LIB}, Guile-Lib} @geq{} 0.2.5

@item
@uref{@value{UGTK3}, Gtk-3.0} @geq{} 3.10.0
@end itemize


@strong{* Examples Dependencies}

@emph{ -- Gtk-4.0 examples -- }

G-Golf currently needs the following additional software to run its
Gtk-4.0 examples:

@itemize @bullet
@item
@uref{@value{UGTK4}, Gtk-4.0} @geq{} 4.10.0

@item
@uref{@value{UGUILE-CAIRO}, Guile-Cairo} > 1.11.2

G-Golf actually requires a patched version of guile-cairo that contains
the following new interface (which is not in guile-cairo 1.11.2):
@code{cairo-pointer->context}.
@end itemize

@emph{ -- Adwaita examples -- }

G-Golf currently needs the following additional software to run its
Adw-1 examples:

@itemize @bullet
@item
@uref{@value{UADW1}, Adw-1} @geq{} 1.7.0
@end itemize


@subheading Install from the tarball

Assuming you have satisfied the dependencies, open a terminal and
proceed with the following steps:

@example
cd <download-path>
tar zxf g-golf-@value{VERSION}.tar.gz
cd g-golf-@value{VERSION}
./configure [--prefix=/your/prefix] [--with-guile-site]
make
make install
@end example

Happy @uref{@value{UG-GOLF}, G-Golf}!


@subheading Install from the source

@uref{@value{UG-GOLF}, G-Golf} uses @uref{@value{UGIT}, Git} for
revision control, hosted on @uref{@value{UG-GOLF-SAVANNAH}, Savannah},
you may browse the sources repository @uref{@value{UG-GOLF-GIT}, here}.

There are currently 2 [important] branches: @code{master} and
@code{devel}. @uref{@value{UG-GOLF}, G-Golf} stable branch is
master, developments occur on the devel branch.

So, to grab, compile and install from the source, open a terminal and:

@example
git clone git://git.savannah.gnu.org/g-golf.git
cd g-golf
./autogen.sh
./configure [--prefix=/your/prefix] [--with-guile-site]
make
make install
@end example

The above steps ensure you're using @uref{@value{UG-GOLF}, G-Golf}
bleeding edge @code{stable} version. If you wish to participate to
developments, checkout the @code{devel} branch:

@example
git checkout devel
@end example

Happy @code{hacking!}


@*
@strong{Notes:}

@enumerate
@item
The @code{default} and @code{--prefix} installation locations for source
modules and compiled files (in the absence of
@code{--with-guile-site}) are:

@example
$(datadir)/g-golf
$(libdir)/g-golf/guile/$(GUILE_EFFECTIVE_VERSION)/site-ccache
@end example

If you pass @code{--with-guile-site}, these locations become:

@example
Guile global site directory
Guile site-ccache directory
@end example

@item
The configure step reports these locations as the content of the
@code{sitedir} and @code{siteccachedir} variables.

After installation, you may consult these variables using pkg-config:

@example
pkg-config g-golf-1.0 --variable=sitedir
pkg-config g-golf-1.0 --variable=siteccachedir
@end example

@item
Unless you have used @code{--with-guile-site}, or unless these locations
are already ’known’ by Guile, you will need to define or augment your
@code{GUILE_LOAD_PATH} and @code{GUILE_COMPILED_PATH} environment
variables accordingly (or @code{%load-path} and
@code{%load-compiled-path} at run time if you prefer@footnote{In this
case, you may as well decide to either alter your @file{$HOME/.guile}
personal file, or, if you are working in a mult-user environmet, you may
also opt for a global configuration. In this case, the file must be
named @file{init.scm} and placed it here (evaluate the following
expression in a terminal): @code{guile -c "(display
(%global-site-dir))(newline)"}.}  (See @uref{@value{UGUILE-ENV-VARS},
Environment Variables} and @uref{@value{UGUILE-LOAD-PATH}, Load Path} in
the Guile Reference Manual).
@ifhtml
@*@*
@end ifhtml

@item
G-Golf also installs its @code{libg-golf.*} library files, in
@code{$(libdir)}. The configure step reports its location as the content
of the @code{libdir} variable, which depends on on the content of the
@code{prefix} and @code{exec_prefix} variables (also reported).

After installation, you may consult these variables using pkg-config:

@example
pkg-config g-golf-1.0 --variable=prefix
pkg-config g-golf-1.0 --variable=exec_prefix
pkg-config g-golf-1.0 --variable=libdir
@end example

@item
Unless the @code{$(libdir)} location is already 'known' by your system,
you will need - to either define or augment your @code{$LD_LIBRARY_PATH}
environment variable, or alter the @file{/etc/ld.so.conf} (or add a file
in @file{/etc/ld.so.conf.d}) and run (as root) @code{ldconfig}, so that
G-Golf finds its @code{libg-golf.*} library files@footnote{Contact your
administrator if you opt for the second solution but don't have
@code{write} priviledges on your system.}.
@ifhtml
@*@*
@end ifhtml

@item
To install G-Golf, you must have write permissions to the default or
@code{$(prefix)} directory and its subdirs, as well as to both Guile's
site and site-ccache directories if @code{--with-guile-site} was
passed.
@ifhtml
@*@*
@end ifhtml

@item
Like for any other GNU Tool Chain compatible software, you may install
the documentation locally using @code{make install-info}, @code{make
install-html} and/or @code{make install-pdf}.
@ifhtml
@*@*
@end ifhtml

@item
G-Golf comes with a @code{test-suite}, which we recommend you to run
(especially before @ref{Reporting Bugs}):

@example
make check
@end example

@item
To try/run an uninstalled version of G-Golf, use the pre-inst-env
script:

@example
./pre-inst-env your-program [arg1 arg2 ...]
@end example
@end enumerate


@node Contact Information
@section Contact Information


@subheading Mailing list

G-Golf uses Guile's mailing lists:

@itemize @bullet
@item @email{guile-user@@gnu.org} is for general user help and
discussion.
@ifhtml
@*@*
@end ifhtml

@item @email{guile-devel@@gnu.org} is used to discuss most aspects
of G-Golf, including development and enhancement requests.
@end itemize

Please use @samp{G-Golf - } to preceed the subject line of G-Golf
related emails, thanks!

You can (un)subscribe to the one or both of these mailing lists by
following instructions on their respective
@uref{@value{UGUILE-LISTINFO}, list information page}.


@subheading IRC

Most of the time you can find me on irc, channel @emph{#guile},
@emph{#guix} and @emph{#scheme} on @emph{irc.libera.chat},
@emph{#clutter} and @emph{#introspection} on @emph{irc.gnome.org}, under
the nickname @emph{daviid}.



@node Reporting Bugs
@section Reporting Bugs

G-Golf uses a bug control and manipulation mailserver. You may send your
bugs report here:

@itemize @bullet
@item @email{bug-g-golf@@gnu.org}
@end itemize

You can (un)subscribe to the bugs report list by following instructions
on the @uref{@value{UG-GOLF-BUGS-LISTINFO}, list information
page}.

Further information and a list of available commands are available
@uref{@value{UDEBBUGS-SERVER-CONTROL}, here}.
