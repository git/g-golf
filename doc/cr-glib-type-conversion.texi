@c -*-texinfo-*-
@c This is part of the GNU G-Golf Reference Manual.
@c Copyright (C) 2025 Free Software Foundation, Inc.
@c See the file g-golf.texi for copying conditions.


@c @defindex tl


@node Type Conversion
@subsection Type Conversion

G-Golf GLib Type Conversion API.@*
Type Conversion - portably storing integers in pointer variables


@subheading Procedures

@indentedblock
@table @code
@item @ref{gint-to-pointer}
@item @ref{guint-to-pointer}
@end table
@end indentedblock


@subheading Description

Exposes upstream macros that allow storing integers in pointers. They
only preserve 32 bits of the integer; values outside the range of a
32-bit integer will be mangled.


@subheading Procedures


@anchor{gint-to-pointer}
@deffn Procedure gint-to-pointer

Returns a pointer.

Stuffs an integer into a pointer type.
@end deffn


@anchor{guint-to-pointer}
@deffn Procedure guint-to-pointer

Returns a pointer.

Stuffs an unsigned integer into a pointer type.
@end deffn
